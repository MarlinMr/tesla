#!/usr/bin/python3
import teslapy, math, json

tesla_config_file = '/home/user/.config/tesla/tesla.json'
tesla_cache_file = '/home/user/.config/tesla/cache.json'
mile = 1.609344

with open(tesla_config_file) as f:
	data = json.load(f)

#with teslapy.Tesla(email=data["email"], password=data["password"], cache_file=tesla_cache_file) as tesla:
with teslapy.Tesla(email=data["email"], cache_file=tesla_cache_file) as tesla:
	tesla.fetch_token()
	vehicles = tesla.vehicle_list()
	vehicle = vehicles[0]

def get_data():
	print("🚗 is " + vehicle["state"] + ". Fetching data...")
	vehicle_data = vehicle.get_vehicle_data()
	print(vehicle_data)
	charge_state = vehicle_data['charge_state']
	climate_state = vehicle_data['climate_state']
	drive_state = vehicle_data['drive_state']
	print("State : " + str(charge_state["charging_state"]))
	print("Power : " + str(charge_state["charger_power"]) + " kW (" +  str(charge_state["charger_phases"]) + "-fas, " + str(charge_state["charger_voltage"]) + "V)")
	print("Power2: " + str(drive_state["power"]) + " kW")
	print("Level : " + str(charge_state["battery_level"]) + " % (" + str(math.floor(charge_state["battery_range"]*mile)) + " km ideal range)")
	print("Added : " + str(charge_state["charge_energy_added"]) + " kWh (" + str(math.floor(charge_state["charge_miles_added_ideal"]*mile)) + " km)")
	print("Inside Temp  : " + str(climate_state["inside_temp"]) + " C ")
	print("Outside Temp : " + str(climate_state["outside_temp"]) + " C ")
	print("Activate air conditioning?")
	ans = input()
	if (ans == 'yes'):
		print(vehicle.command("CLIMATE_ON"))

if (vehicle["state"] == 'online'):
	get_data()

elif (vehicle["state"] == 'asleep'):
	print("🚗 is " + vehicle["state"] + ". Wake?")
	ans = input()
	if (ans == 'yes'):
		print("⏰🚗")
		vehicle.sync_wake_up()
		get_data()
else:
	print("🏳️")
